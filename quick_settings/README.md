# `PhoshQuickSettingsBox`

`PhoshQuickSettingsBox` displays its children in an order of columns, creating a
new row when necessary. It is inspired by
[`GtkFlowBox`](https://docs.gtk.org/gtk3/class.FlowBox.html). The child of the
box should be `PhoshQuickSettingsChild`.

![Demo of `PhoshQuickSettingsBox`](1.png)
![Demo of `PhoshQuickSettingsBox`](2.png)

The algorithm and working is documented inside the files.

## Getting Started

You need [`PyGObject`](https://pygobject.readthedocs.io/) to run the Python code with Gtk 3.

1. Clone the repository.

``` sh
git@ssh.gitlab.gnome.org:arun-mani-j/phosh-mocks.git
```

2. Go inside the directory.

``` sh
cd phosh-mocks
```

3. Run the module.

``` sh
python3 -m quick_settings
```

## To Do
- Understand what CSS properties should be used.
