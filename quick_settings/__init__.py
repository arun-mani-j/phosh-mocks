import gi

gi.require_version("Gtk", "3.0")

from quick_settings.quick_settings_child import QuickSettingsChild
from quick_settings.quick_settings_box import QuickSettingsBox
