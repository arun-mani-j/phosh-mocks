"""
Contains `PhoshQuickSettingsChild`.
"""

import logging
from typing import Callable
from gi.repository import Gtk, GObject

logger = logging.getLogger(__name__)


class QuickSettingsChild(Gtk.Bin):
    """
    `PhoshQuickSettingsChild` is used as a child of `PhoshQuickSettingsBox`.

    A child has a label to display text that explains its purpose. It can be
    activated on and off. Use `active` property to set it manually. The child
    has a widget in `status` property which should be shown and hidden on
    signals defined below. Use `can_show_status` to set if the `status` can be
    allowed to shown. Though, it is better to use `PhoshQuickSettingsBox`'s
    `can_show_status` if all the children's must be set at the same time.

    The child emits `clicked` signal when it is clicked.
    It emits `show-status` signal when its status should be shown.
    It emits `hide-status` signal when its status should be hidden.
    """

    __gtype_name__ = "PhoshQuickSettingsChild"

    _label: str = ""
    _active: bool | None = None
    _can_show_status: bool | None = None
    _status: Gtk.Widget | None = None
    _folded: bool = True
    _label_btn: Gtk.Button
    _image_btn: Gtk.Button
    _box: Gtk.Box

    def __init__(
        self,
        label: str = "",
        active: bool = False,
        status: Gtk.Widget | None = None,
        can_show_status: bool = True,
        **kwargs
    ):
        self._label_btn = Gtk.Button(hexpand=True, visible=True)
        self._image_btn = Gtk.Button()
        self._box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, visible=True)
        self._label_btn.connect("clicked", lambda *_: self.emit("clicked"))
        self._image_btn.connect("clicked", lambda *_: self._toggle_fold())
        Gtk.Bin.__init__(
            self,
            name="quick-settings-child",
            label=label,
            active=active,
            status=status,
            can_show_status=can_show_status,
            **kwargs,
        )
        self.set_css_name("quick-settings-child")
        self._box.add(self._label_btn)
        self._box.add(self._image_btn)
        self.add(self._box)

    @GObject.Property(type=str)
    def label(self) -> str:
        """
        Get the label of the child.
        """
        return self._label

    @label.setter
    def label(self, label: str):
        """
        Set the label of the child.
        """
        if self._label == label:
            return

        self._label = label
        self._label_btn.set_label(label)

    @GObject.Property(type=bool, default=False)
    def active(self) -> bool:
        """
        Get if the child is active.
        """
        return self._active

    @active.setter
    def active(self, active: bool):
        """
        Set if the child is active
        """
        if self._active == active:
            return

        self._active = active

    @GObject.Property(type=bool, default=True)
    def can_show_status(self) -> bool:
        """
        Get if status can be shown.
        """
        return self._can_show_status

    @can_show_status.setter
    def can_show_status(self, can_show_status: bool):
        """
        Set if status can be shown.

        If the child is already showing its status, then setting `False` will
        hide it.
        """
        if self._can_show_status == can_show_status:
            return

        self._can_show_status = can_show_status

        if not self._status:
            return

        if can_show_status:
            self._image_btn.set_visible(True)
        else:
            self._image_btn.set_visible(False)

        if not self._folded and not can_show_status:
            self.emit("hide-status")

    @GObject.Property(
        type=Gtk.Widget,
        flags=GObject.ParamFlags.CONSTRUCT_ONLY | GObject.ParamFlags.READWRITE,
    )
    def status(self) -> Gtk.Widget:
        """
        Get the status of the child.
        """
        return self._status

    @status.setter
    def status(self, status: Gtk.Widget):
        """
        Set the status of the child.
        """
        if self._status == status:
            return

        self._status = status

        if status is None:
            self._image_btn.set_visible(False)
        else:
            self._set_folded_icon("go-down-symbolic")
            self._image_btn.set_visible(True)

    @GObject.Signal
    def clicked(self):
        """
        Emit the `clicked signal.`
        """
        return

    @GObject.Signal
    def show_status(self):
        """
        Emit the `show-status` signal.
        """
        self._set_folded_icon("go-up-symbolic")
        self._folded = False

    @GObject.Signal
    def hide_status(self):
        """
        Emit the `hide-status` signal.
        """
        self._set_folded_icon("go-down-symbolic")
        self._folded = True

    def _set_folded_icon(self, icon_name: str):
        """
        Set the state of folding to give icon name.
        """
        image = Gtk.Image(icon_name=icon_name)
        self._image_btn.set_image(image)

    def _toggle_fold(self):
        """
        Toggle the fold of status.
        """
        if self._folded:
            self.emit("show-status")
        else:
            self.emit("hide-status")
