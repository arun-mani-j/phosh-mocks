"""
Contains `PhoshQuickSettingsBox`.

Public child means the child of the box that can be added and removed by the user.
Internal child means the child used by the box that is unavailable to the user.

Unless otherwise mentioned, plain "children" means the public children.
"""

import math
import logging
from typing import Callable

from gi.repository import Gtk, GObject, Gdk

from quick_settings import QuickSettingsChild

logger = logging.getLogger(__name__)


class QuickSettingsBox(Gtk.Container):
    """
    `PhoshQuickSettingsBox` arranges its children in a number of columns and
    rows. The number of columns is adaptive in nature, making the box
    responsive.

    The maximum number of columns can be set using `columns` property.
    `PhoshQuickSettingsBox` is similar to `GtkFlowBox` except that it can show
    the status of a child below its row. As a consequence,
    `PhoshQuickSettingsBox` accepts only `PhoshQuickSettingsChild`.

    When a child signals to show its status, the box shows it below the child's
    row. At a time, only one child's status can be shown at max, so the box
    takes care of this functionality automatically.

    Sometimes, it may not be required to show status, like in locked screen. Use
    `can_show_status` property to set it.

    `spacing` can be used to specify the space between the children.
    """

    __gtype_name__ = "PhoshQuickSettingsBox"

    _columns: int = 1
    _spacing: int = 0
    _can_show_status: bool | None = None
    _children: list[Gtk.Widget] = []
    _revealer: Gtk.Revealer
    _revealed_child: QuickSettingsChild | None = None
    _revealed_index: int = 0

    def __init__(
        self,
        columns: int = 1,
        spacing: int = 0,
        can_show_status: bool = True,
        **kwargs,
    ):
        Gtk.Container.__init__(
            self,
            name="quick_settings_box",
            columns=columns,
            spacing=spacing,
            can_show_status=can_show_status,
            **kwargs,
        )
        self.set_css_name("quick-settings-box")
        self.set_has_window(False)
        self._revealer = Gtk.Revealer(hexpand=True, visible=True)
        self._revealer.set_parent(self)

    @GObject.Property(type=int, minimum=1, default=1)
    def columns(self):
        """
        Get the maximum number of columns the box can use.
        """
        return self._columns

    @columns.setter
    def columns(self, columns: int):
        """
        Set the maximum number of columns the box can use.
        """
        if self._columns == columns:
            return

        self._columns = columns
        self.queue_resize()

    @GObject.Property(type=int, minimum=0, default=0)
    def spacing(self):
        """
        Get the space between children of the box.
        """
        return self._spacing

    @spacing.setter
    def spacing(self, spacing: int):
        """
        Set the space between children of the box.
        """
        if self._spacing == spacing:
            return

        self._spacing = spacing
        self.queue_resize()

    @GObject.Property(type=bool, default=True)
    def can_show_status(self):
        """
        Get if status is allowed to be shown.

        Useful in scenarios like locked screen, where status is typically not
        shown.
        """
        return self._can_show_status

    @can_show_status.setter
    def can_show_status(self, can_show_status: bool):
        """
        Set if status is allowed to be shown.
        """
        if self._can_show_status == can_show_status:
            return

        self._can_show_status = can_show_status

        if not can_show_status and self._revealed_child:
            self._revealed_child.emit("hide-status")

        for child in self._children:
            child.can_show_status = can_show_status

    def _show_status(self, child: QuickSettingsChild, i: int):
        """
        Show the status of child at given linear index.

        If another child's status is shown, then it is hidden before showing the
        new one.
        """
        if self._revealed_child is not None:
            self._revealed_child.emit("hide-status")

        self._revealed_child = child
        self._revealed_index = i
        self._revealer.add(child.status)
        self._revealer.set_reveal_child(True)

    def _hide_status(self, child: QuickSettingsChild, _i: int):
        """
        Hide the status of child at given linear index.
        """
        self._revealed_child = None
        self._revealed_index = 0
        self._revealer.set_reveal_child(False)
        self._revealer.remove(child.status)

    def do_add(self, widget: Gtk.Widget):
        """
        Add the widget to the box.

        The widget should of type `QuickSettingsChild`.
        """
        if not isinstance(widget, QuickSettingsChild):
            raise TypeError(f"Expected {QuickSettingsChild} but got {widget}")

        self._children.append(widget)
        i = len(self._children) - 1
        widget.connect(
            "show-status",
            lambda *_: self._show_status(widget, i),
        )
        widget.connect(
            "hide-status",
            lambda *_: self._hide_status(widget, i),
        )
        widget.set_parent(self)
        self.queue_resize()

    def do_remove(self, widget: Gtk.Widget):
        """
        Remove the widget from the box.
        """
        # TODO: Disconnect the signals. Should be easy to do in C via the
        # `disconnect_by_data` etc. functions.
        self._children.remove(widget)
        widget.unparent()
        self.queue_resize()

    def do_forall(self, internal: bool, callback: Callable):
        """
        Call the given callback for every child of the box.

        If `internal` is `True`, then calls on internal children too.
        """
        for child in self._children:
            callback(child)

        if internal:
            callback(self._revealer)

    def _compute_child_height(self) -> int:
        """
        Compute the height of a child's cell in the box.

        It is the maximum of natural height of all children.
        """
        height = 0

        for child in self._children:
            result = child.get_preferred_height()
            height = max(height, result.natural_height)

        return height

    def _compute_child_width(self) -> int:
        """
        Compute the width of a child's cell in the box.

        It is the maximum of natural width of all children.
        """
        width = 0

        for child in self._children:
            result = child.get_preferred_width()
            width = max(width, result.natural_width)

        return width

    def do_get_request_mode(self) -> Gtk.SizeRequestMode:
        """
        Get the preferred size request mode.
        """
        return Gtk.SizeRequestMode.HEIGHT_FOR_WIDTH

    def do_get_preferred_height_for_width(self, width: int) -> tuple[int, int]:
        """
        Get the preferred minimum and natural height for given width.

        1. Compute the width and height of a child's cell.
        2. Compute the number of columns that can fill the given width where
           each column is of child's width.
        3. Compute the number of rows for the above columns.
        4. If status should be shown, then add up it's height too.

        Computing width:
        - 1 column needs 0 spacing.
        - 2 columns need 1 spacing.
        - 3 columns need 2 spacing.
        - `cols` columns need `cols - 1` spacing.

        width = cols * child_width + (cols - 1) * spacing
        width = cols * child_width + cols * spacing - spacing
        width = cols * (child_width + spacing) - spacing

        Computing number of columns:
        width           = cols * (child_width + spacing) - spacing
        width + spacing = cols * (child_width + spacing)
        cols            = (width + spacing) / (child_width + spacing)
        """
        if not self._children:
            return (0, 0)

        child_width = self._compute_child_width()
        child_height = self._compute_child_height()
        cols = math.floor((width + self._spacing) / (child_width + self._spacing))
        rows = math.ceil(len(self._children) / cols)

        nat_height = rows * (child_height + self._spacing) - self._spacing
        min_height = nat_height

        if self._revealed_child:
            rev = self._revealer.get_preferred_height_for_width(width)
            nat_height += rev.natural_height + self._spacing
            min_height += rev.minimum_height + self._spacing

        # logger.debug(
        #     "Giving preferred height for width = %s: minimum = %s, natural = %s",
        #     width,
        #     min_height,
        #     nat_height,
        # )

        return (min_height, nat_height)

    def do_get_preferred_width(self) -> tuple[int, int]:
        """
        Get the preferred minimum and natural width.

        If status is shown, then the width is maximum of columns' and revealer's
        width.
        """
        if not self._children:
            return (0, 0)

        child_width = self._compute_child_width()
        cols = self._columns
        min_width = child_width
        nat_width = cols * (child_width + self._spacing) - self._spacing

        if self._revealed_child:
            rev = self._revealer.get_preferred_width()
            min_width = max(min_width, rev.minimum_width)
            nat_width = max(nat_width, rev.natural_width)

        # logger.debug(
        #     "Giving preferred width: minimum = %s, natural = %s",
        #     min_width,
        #     nat_width,
        # )

        return min_width, nat_width

    def _allocate_children(
        self,
        x: int,
        y: int,
        width: int,
        height: int,
        cols: int,
        rows: int,
        revealer_row: int,
        revealer_width: int,
        revealer_height: int,
    ):
        """
        Allocate a space of rectangle whose top-left point is `(x, y)` and is of
        given width and height to the given columns and rows.

        If the revealer is shown, then allocates a region of given width and
        height below the given row.
        """
        # logger.debug(
        #     (
        #         "Allocating children: x = %s, y = %s, width = %s, height = %s, "
        #         "cols = %s, rows = %s, revealer_row = %s, revealer_width = %s, "
        #         "revealer_height = %s"
        #     ),
        #     x,
        #     y,
        #     width,
        #     height,
        #     cols,
        #     rows,
        #     revealer_row,
        #     revealer_width,
        #     revealer_height,
        # )

        rect = Gdk.Rectangle()
        rect.x = x
        rect.y = y
        rect.width = 0
        rect.height = 0
        self._revealer.size_allocate(rect)  # To invalidate old allocation
        rect.width = width
        rect.height = height

        for row in range(rows):
            for col in range(cols):
                i = row * cols + col
                if i == len(self._children):
                    break
                child = self._children[i]
                child.size_allocate(rect)
                rect.x += width + self._spacing

            rect.x = x
            rect.y += height + self._spacing

            if self._revealed_child and row == revealer_row:
                rect.height = revealer_height - self._spacing
                rect.width = revealer_width
                self._revealer.size_allocate(rect)
                rect.height = height
                rect.width = width
                rect.y += revealer_height

    def do_size_allocate(self, allocation: Gdk.Rectangle):
        """
        Allocate the given allocation to internal and public children of box.

        The number of columns is always less than the box's columns property.
        When more width is available, then it is equally distributed among the
        columns. The same is done for heights too.
        """
        Gtk.Container.do_size_allocate(self, allocation)

        if not self._children:
            return

        # logger.debug(
        #     "Got an allocation of: width = %s, height = %s",
        #     allocation.width,
        #     allocation.height,
        # )

        if self._revealed_child:
            rev_height = self._revealer.get_preferred_height_for_width(allocation.width)
            rev_height = rev_height.natural_height + self._spacing
        else:
            rev_height = 0

        child_width = self._compute_child_width()
        child_height = self._compute_child_height()

        cols = math.floor(allocation.width / child_width)
        cols = min(cols, self._columns)
        rows = math.ceil(len(self._children) / cols)

        total_width = cols * (child_width + self._spacing) - self._spacing
        extra_space = allocation.width - total_width
        child_width += extra_space // cols

        total_height = rows * (child_height + self._spacing) - self._spacing
        extra_space = allocation.height - total_height
        revealer_height = min(extra_space, rev_height)
        extra_space = allocation.height - total_height - revealer_height
        child_height += extra_space // rows

        revealer_row = math.floor(self._revealed_index / cols)
        revealer_width = allocation.width

        self._allocate_children(
            allocation.x,
            allocation.y,
            child_width,
            child_height,
            cols,
            rows,
            revealer_row,
            revealer_width,
            revealer_height,
        )
