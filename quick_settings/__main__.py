import logging
from gi.repository import Gtk
from quick_settings import QuickSettingsBox, QuickSettingsChild

logging.basicConfig(
    format="%(asctime)s :: %(filename)s :: %(message)s", level=logging.DEBUG
)


def toggle_can_show_status(toggle: Gtk.ToggleButton, box: QuickSettingsBox):
    box.can_show_status = not toggle.get_active()


def on_activate(app: Gtk.Application):
    win = Gtk.ApplicationWindow(application=app)
    vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=20, visible=True)
    toggle = Gtk.ToggleButton(label="Hide Status", visible=True)
    label = Gtk.Label(label="-", visible=True)
    box = QuickSettingsBox(
        columns=6, spacing=10, hexpand=True, vexpand=True, visible=True
    )

    for i in range(10):
        child = QuickSettingsChild(
            label=f"Button {i}",
            status=(
                Gtk.Label(label=f"Status of {i}", visible=True, hexpand=True)
                if i % 2 == 0
                else None
            ),
            visible=True,
        )
        child.connect(
            "clicked", lambda w, i: label.set_label(f"Child {i} activated."), i
        )
        box.add(child)

    toggle.connect("toggled", toggle_can_show_status, box)
    vbox.add(box)
    vbox.add(toggle)
    vbox.add(label)
    box.set_size_request(300, 100)
    win.add(vbox)
    win.present()


app = Gtk.Application(application_id="sm.puri.quick_settings")
app.connect("activate", on_activate)
app.run([])
