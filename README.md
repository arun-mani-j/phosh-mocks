# Phosh Mocks

A collection of prototypes, ideas, designs and thoughts for Phosh and its
ecosystem.

Check the individual folder for each mock.

1. [`QuickSettingsBox`](/quick_settings) - an adaptive box to display quick settings and show their
   status page.
